// Separation of Concerns
/* 
    Why do we have to?
        - Better code readability
        - Improved scalability
        - Better code maintainability

    What do we separate
        - Models
            - Contains WHAT objects are needed in our API
                (ex.: users, courses)
            - Object Schemas

        - COntrollers
            - Contains instructions on HOW your API will perform its intended tasks
            - Mongoose model queries are used here, examples are:
                Model.find()
                Model.findByIdAndDelete()
                Model.findByIdAndUpdate()
                Model.findByIdAndRemove()
        
        - Routes
            - Defines WHEN particular controllers will be used            

*/

// JS Modules
/* 
    

*/


// Activity
/* 
    1. Create a route for getting a specific task.
    2. Create a controller function for retrieving a specific task.
    3. Return the result back to the client/Postman.
    4. Process a GET request at the /tasks/:id route using postman to get a specific task.
    5. Create a route for changing the status of a task to complete.
    6. Create a controller function for changing the status of a task to complete.
    7. Return the result back to the client/Postman.
    8. Process a PUT request at the /tasks/:id/complete route using postman to update a task.
    9. Create a git repository named s36.
    10. Initialize a local git repository, add the remote link and push to git with the commit message of "Added s36 activity code"
    11. Add the link in Boodle.
    12. Send a screenshot of the /tasks/:id and /task/:id/complete route in Postman to our batch hangouts

*/