// Controllers contain the functions and business logic of our Express JS application
// Means that all the operations it can do will be placed in this file

// const task = require("../models/task.js");
const Task = require("../models/task.js");

// Controller function for getting all the tasks

module.exports.getAllTasks = () => {
    return Task.find({}).then(result => {
        return result;
    })
};

// Activity objective #2
// Controller function for retrieving a specific task.
module.exports.getTask = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(error);
            return false;
        } else{
            return result;
        }
    })
}

// Create a task (POST)
module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name : requestBody.name
    })
    return newTask.save().then((task,error) => {
        if (error) {
            console.log(error);
            return false;
        } else {
            return task;
        }
    })
};

// Delete single task
module.exports.deleteTask = (taskId) => {
    return Task.findByIdAndRemove(taskId).then((removedTask,err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            return removedTask;
        }
    }) 
};

// Update task name
module.exports.updateTask = (taskId, newContent) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(err);
            return false;
        }
        result.name = newContent.name;
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTask;
            }
        })
    })
};

// Activity Objective #6
// Controller for changing the status from "pending" to "complete"
module.exports.updateStatus = (taskId) => {
    return Task.findById(taskId).then((result, error) => {
        if (error) {
            console.log(err);
            return false;
        }
        result.status = "complete";
        return result.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.log(saveErr);
                return false;
            } else {
                return updatedTask;
            }
        })
    })
};